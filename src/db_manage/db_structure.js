import Sequelize from 'sequelize'

export default [
  {
    keys: {
      motion_id : Sequelize.INTEGER,
      root_id : Sequelize.INTEGER,
      title  : Sequelize.STRING,
      content: Sequelize.STRING,
      quantity : Sequelize.STRING,
      proposer : Sequelize.STRING,
      section : Sequelize.INTEGER,
      takecoin_num : Sequelize.INTEGER,
      approve_end_block_num : Sequelize.INTEGER,
      extern_data: Sequelize.STRING,
      trx_id : Sequelize.STRING,
      is_confirmed : Sequelize.BOOLEAN,
      approved : Sequelize.STRING,
      requested : Sequelize.STRING,
      unapproved : Sequelize.STRING,
      is_deleted: Sequelize.BOOLEAN,
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'motions'
    }
  },
  {
    keys: {
      motion_id   : Sequelize.INTEGER,
      action_name : Sequelize.STRING,
      approver    : Sequelize.STRING,
      memo        : Sequelize.STRING,
      is_confirmed: Sequelize.BOOLEAN,
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'motion_apporoved'
    }
  },
]