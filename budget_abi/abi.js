export default {
    "account_name": "eosc.budget",
    "abi":
    {
        "version": "eosio::abi/1.1",
        "types": [],
        "structs": [
        {
            "name": "agreecoin",
            "base": "",
            "fields": [
            {
                "name": "approver",
                "type": "account_name"
            },
            {
                "name": "proposer",
                "type": "account_name"
            },
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "memo",
                "type": "string"
            }]
        },
        {
            "name": "approval_info",
            "base": "",
            "fields": [
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "requested",
                "type": "account_name[]"
            },
            {
                "name": "approved",
                "type": "account_name[]"
            },
            {
                "name": "unapproved",
                "type": "account_name[]"
            }]
        },
        {
            "name": "approve",
            "base": "",
            "fields": [
            {
                "name": "approver",
                "type": "account_name"
            },
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "memo",
                "type": "string"
            }]
        },
        {
            "name": "budget_config",
            "base": "",
            "fields": [
            {
                "name": "config_name",
                "type": "name"
            },
            {
                "name": "number_value",
                "type": "uint64"
            },
            {
                "name": "string_value",
                "type": "string"
            }]
        },
        {
            "name": "closecoin",
            "base": "",
            "fields": [
            {
                "name": "proposer",
                "type": "account_name"
            },
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "memo",
                "type": "string"
            }]
        },
        {
            "name": "closemotion",
            "base": "",
            "fields": [
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "memo",
                "type": "string"
            }]
        },
        {
            "name": "committee_info",
            "base": "",
            "fields": [
            {
                "name": "budget_name",
                "type": "name"
            },
            {
                "name": "member",
                "type": "account_name[]"
            }]
        },
        {
            "name": "handover",
            "base": "",
            "fields": [
            {
                "name": "committeers",
                "type": "account_name[]"
            },
            {
                "name": "memo",
                "type": "string"
            }]
        },
        {
            "name": "motion_info",
            "base": "",
            "fields": [
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "root_id",
                "type": "uint64"
            },
            {
                "name": "title",
                "type": "string"
            },
            {
                "name": "content",
                "type": "string"
            },
            {
                "name": "quantity",
                "type": "asset"
            },
            {
                "name": "proposer",
                "type": "account_name"
            },
            {
                "name": "section",
                "type": "uint32"
            },
            {
                "name": "takecoin_num",
                "type": "uint32"
            },
            {
                "name": "approve_end_block_num",
                "type": "uint32"
            },
            {
                "name": "extern_data",
                "type": "bytes[]"
            }]
        },
        {
            "name": "propose",
            "base": "",
            "fields": [
            {
                "name": "proposer",
                "type": "account_name"
            },
            {
                "name": "title",
                "type": "string"
            },
            {
                "name": "content",
                "type": "string"
            },
            {
                "name": "quantity",
                "type": "asset"
            }]
        },
        {
            "name": "takecoin",
            "base": "",
            "fields": [
            {
                "name": "proposer",
                "type": "account_name"
            },
            {
                "name": "montion_id",
                "type": "uint64"
            },
            {
                "name": "content",
                "type": "string"
            },
            {
                "name": "quantity",
                "type": "asset"
            }]
        },
        {
            "name": "takecoin_motion_info",
            "base": "",
            "fields": [
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "montion_id",
                "type": "uint64"
            },
            {
                "name": "content",
                "type": "string"
            },
            {
                "name": "quantity",
                "type": "asset"
            },
            {
                "name": "receiver",
                "type": "account_name"
            },
            {
                "name": "section",
                "type": "uint32"
            },
            {
                "name": "end_block_num",
                "type": "uint32"
            },
            {
                "name": "requested",
                "type": "account_name[]"
            },
            {
                "name": "approved",
                "type": "account_name[]"
            },
            {
                "name": "unapproved",
                "type": "account_name[]"
            }]
        },
        {
            "name": "turndown",
            "base": "",
            "fields": [
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "memo",
                "type": "string"
            }]
        },
        {
            "name": "unagreecoin",
            "base": "",
            "fields": [
            {
                "name": "approver",
                "type": "account_name"
            },
            {
                "name": "proposer",
                "type": "account_name"
            },
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "memo",
                "type": "string"
            }]
        },
        {
            "name": "unapprove",
            "base": "",
            "fields": [
            {
                "name": "approver",
                "type": "account_name"
            },
            {
                "name": "id",
                "type": "uint64"
            },
            {
                "name": "memo",
                "type": "string"
            }]
        },
        {
            "name": "updateconfig",
            "base": "",
            "fields": [
            {
                "name": "config",
                "type": "name"
            },
            {
                "name": "number_value",
                "type": "uint64"
            },
            {
                "name": "string_value",
                "type": "string"
            }]
        }],
        "actions": [
        {
            "name": "agreecoin",
            "type": "agreecoin",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: pass a take coin proposal\nsummary: 'the member of the committee use this to pass a take coin proposal'\n---"
        },
        {
            "name": "approve",
            "type": "approve",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: pass a proposal \nsummary: 'the member of the committee use this to pass a proposal'\n---"
        },
        {
            "name": "closecoin",
            "type": "closecoin",
            "ricardian_contract": ""
        },
        {
            "name": "closemotion",
            "type": "closemotion",
            "ricardian_contract": ""
        },
        {
            "name": "handover",
            "type": "handover",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: hand over the member of Committee\nsummary: 'clear the former member of the committee and add new member of the committee'\n---"
        },
        {
            "name": "propose",
            "type": "propose",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: Add a new proposal \nsummary: 'user can add a new propose with this action'\n---"
        },
        {
            "name": "takecoin",
            "type": "takecoin",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: add a take coin motion\nsummary: 'user can add a takecoin motion with this action'\n---"
        },
        {
            "name": "turndown",
            "type": "turndown",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: trun down a motion\nsummary: 'the contract can turn down the motion'\n---"
        },
        {
            "name": "unagreecoin",
            "type": "unagreecoin",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: Reject a take coin proposal\nsummary: 'the member of the committee use this to reject a take coin proposal'\n---"
        },
        {
            "name": "unapprove",
            "type": "unapprove",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: Reject a proposal\nsummary: 'the member of the committee use this to reject a proposal'\n---"
        },
        {
            "name": "updateconfig",
            "type": "updateconfig",
            "ricardian_contract": "---\nspec_version: \"0.2.0\"\ntitle: set config on budget\nsummary: 'set some config on this contract'\n---"
        }],
        "tables": [
        {
            "name": "approvers",
            "index_type": "i64",
            "key_names": [],
            "key_types": [],
            "type": "approval_info"
        },
        {
            "name": "budgetconfig",
            "index_type": "i64",
            "key_names": [],
            "key_types": [],
            "type": "budget_config"
        },
        {
            "name": "committee",
            "index_type": "i64",
            "key_names": [],
            "key_types": [],
            "type": "committee_info"
        },
        {
            "name": "motions",
            "index_type": "i64",
            "key_names": [],
            "key_types": [],
            "type": "motion_info"
        },
        {
            "name": "takecoins",
            "index_type": "i64",
            "key_names": [],
            "key_types": [],
            "type": "takecoin_motion_info"
        }],
        "ricardian_clauses": [],
        "error_messages": [],
        "abi_extensions": [],
        "variants": []
    }
}